import * as fs from "fs";
import * as path from "path";
import { fileURLToPath } from "url";

export function getSchema() {
    const dirname = path.dirname(fileURLToPath(new URL(import.meta.url)));
    const schemaSql = fs.readFileSync(path.resolve(dirname, "schema.sql"), "utf8");
    return schemaSql;
}
