FROM postgres:12-alpine3.14

COPY out/schema.sql /docker-entrypoint-initdb.d/schema.sql
